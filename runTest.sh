#!/bin/bash

g++ -o program main.cpp foo.cpp -g

./program

if [ $? -eq 0 ]; then
    echo "Program executed successfully."
else
    echo "Program failed to execute."
fi
