
# C++ Project with CI/CD and Code Coverage

## Overview

So basically this project demonstrates a simple implementation of Continuous Integration (CI), unit testing, and code coverage analysis for a basic C++ application using GitLab CI/CD. The application includes a simple function `foo` that behaves differently based on the input value (negative, zero, or positive) and is tested via a main program.

## Project Structure

- `foo.h and foo.cpp`: Declares and defines a simple function foo.
- `main.cpp`: Contains the main function that calls foo with predetermined values.
- `runTest.sh`: A bash script that serves as a basic unit test by compiling and running the main program.
- `.gitlab-ci.yml`: Defines the CI pipeline for building the project, running tests, and generating a code coverage report.

## Continuous Integration Pipeline

The CI pipeline consists of the following stages:
1. **Build**: Compiles the main program with debugging symbols.
2. **Test**: Runs the runTest.sh script to execute the compiled program.
3. **Coverage**: Generates a coverage report using lcov and saves it as a CI job artifact.


## Code Coverage Report

The project integrates code coverage analysis to assess the extent to which the source code is executed during testing. The coverage report highlights:
- Overall test coverage percentage.
- Line and function coverage for each source file.
- Areas of the code not executed by current tests, pointing to potential gaps in testing.

### Coverage Report Highlights

- **`foo.cpp` Coverage**: Achieved 85.7% line coverage. This high coverage indicates that the majority of `foo`'s logic was tested, especially the paths for zero and positive values. The untested paths likely correspond to the negative input scenario, suggesting an area for improvement in our testing strategy.
- **`main.cpp` Coverage**: Reached 100% line coverage, confirming that all executable lines in the `main` function were tested. This is expected, as the `main` function's sole responsibility in this project is to call the `foo` function with specific values.

## Tracking Coverage Over Time

This is just an Idea I came up with, not quite sure if this will work tho.

- **Version Control for Summaries**: After each CI run, key coverage metrics are extracted and version-controlled in `COVERAGE_SUMMARY.md`.
- **Trend Analysis Tools**: We utilize [Codecov](https://codecov.io/) for in-depth coverage trend analysis and visualization.
- **Automated Coverage Checks**: Our CI pipeline includes checks to ensure new code meets our coverage standards.
- **Coverage Diff Reporting**: For each commit, coverage differences are reported, highlighting progress or areas needing attention.
- **Regular Audits**: We conduct quarterly coverage audits to reassess our coverage goals and testing strategies.


