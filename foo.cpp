#include "foo.h"
#include <iostream>

void foo(int value) {
    if (value < 0) {
        std::cout << "Negative value" << std::endl;
    } else if (value == 0) {
        std::cout << "Zero" << std::endl;
    } else {
        std::cout << "Positive value" << std::endl;
    }
}
